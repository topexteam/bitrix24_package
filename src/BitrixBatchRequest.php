<?php
namespace Topexperts\Bitrix24WebHook;
use Exception;
trait BitrixBatchRequest
{

    /**
     * Подготовка "batch" запроса
     * @param string $action
     * @param array $search
     * @param array $select
     * @param array $filter
     * @param bool $amout_value
     * @param int $start
     * @param int $count
     * @return array|bool
     */
    public function batch($action = 'crm.lead.list', $search = [], $select = ['ID'], $filter = [], $amout_value = false, $start = 0, $count = 50)
    {
        $batch = [];
        $res = [];
        $i = 0;
        $amout = $amout_value;

        try {
            do {
                $batch['cmd_' . $i] = $action . '?' . http_build_query(
                        array_merge(
                            [
                                'filter' => $filter,
                                'select' => $select,
                                'start' => $start * $count,
                            ],
                            $search
                        )
                    );

                if (!empty($amout_value)) {
                    if ($i % $count == 0) {
                        $res[] = $this->batchRequest(array('cmd' => $batch));
                        $batch = array();
                    }
                } else {
                    $res[] = $this->batchRequest(array('cmd' => $batch));
                    $batch = array();
                }

                $i++;
                $start++;
                $amout = (!empty($amout)) ? $amout : ceil($res[0]['result_total']['cmd_0']);
            } while ($i <= $amout / $count);
        } catch (Exception $e) {
            return false;
        }

        if (!empty($res[0]['result_total']['cmd_0'])) {
            $count = $res[0]['result_total']['cmd_0'];
        }

        return [
            'result' => $res,
            'count' => $count,
        ];

    }

    /**
     * Отправка запроса для requestConstructor() методом "batch"
     * @param $params
     * @return mixed
     */

    public function batchRequest($params)
    {
        $url = 'https://' . $this->url . '/rest/' . $this->ID . '/' . $this->password . '/batch.json';
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_POSTFIELDS => http_build_query($params),
        ));
        $result = curl_exec($curl);
        $result_array = json_decode($result, true);
        curl_close($curl);
        return $result_array['result'];
    }
}
