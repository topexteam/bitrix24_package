<?php

namespace Topexperts\Bitrix24WebHook;
use Exception;

trait BitrixRequest
{
    /**
     * Отправляем запрос на получение данных из Bitrix
     * @param $method
     * @param $params
     * @return bool
     */
    public function request($method, $params)
    {

        try {
            $url = 'https://' . $this->url . '/rest/' . $this->ID . '/' . $this->password . '/' . $method . '.json';
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_POST => 1,
                CURLOPT_HEADER => 0,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_POSTFIELDS => http_build_query($params),
            ));
            $result = curl_exec($curl);
            $result_array = json_decode($result, true);
            curl_close($curl);
        } catch (Exception $e) {

            return false;
        }

        return !empty($result_array['result']) ? $result_array['result'] : false;
    }
}
